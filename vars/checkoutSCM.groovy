def call(body) {
    // evaluate the body block, and collect configuration into the object
    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    def url = config.url
    def creds =  config.credentials
    def branch = config.branch
    
    try{
        checkout([$class: 'GitSCM', branches: [[name: "${branch}"]], doGenerateSubmoduleConfigurations: false, extensions: [], submoduleCfg: [], userRemoteConfigs: [[credentialsId:"${creds}", url: "${url}"]]])
    } catch(err){
        echo "ERROR"
        echo err.getMessage()
    }
}
